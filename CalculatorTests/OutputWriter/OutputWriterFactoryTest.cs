﻿using Xunit;
using Calculator;

namespace CalculatorTests
{
    public class OutputWriterFactoryTest
    {
        [Fact]
        public void ShouldReturnTextFileOutputWriter()
        {
            var outputWriter = OutputWriterFactory.GetFactoryInstance().GetOutputWriterInstance(OutputType.TEXT_FILE);
            Assert.IsType<TextFileOutputWriter>(outputWriter);
        }
    }
}
