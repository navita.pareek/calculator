﻿using Xunit;
using Calculator;
namespace CalculatorTests
{
    public class ExpressionValidatorTest
    {
        [Fact]
        public void ShouldReturnTrueForValidExpression()
        {
            string expression = "7/2*3+5";

            Assert.True(ExpressionValidator.IsValid(expression));
        }

        [Fact]
        public void ShouldReturnTrueForOnlyNumberExpression()
        {
            string expression = "55";

            Assert.True(ExpressionValidator.IsValid(expression));
        }

        [Fact]
        public void ShouldReturnFalseForPreOperatorExpression()
        {
            string expression = "*5";

            Assert.False(ExpressionValidator.IsValid(expression));
        }

        [Fact]
        public void ShouldReturnTrueForPreOperatorAddExpression()
        {
            string expression = "+5";

            Assert.True(ExpressionValidator.IsValid(expression));
        }

        [Fact]
        public void ShouldReturnTrueForPreOperatorSubExpression()
        {
            string expression = "-5";

            Assert.True(ExpressionValidator.IsValid(expression));
        }

        [Fact]
        public void ShouldReturnFalseForPostOperatorExpression()
        {
            string expression = "5*";

            Assert.False(ExpressionValidator.IsValid(expression));
        }

        [Fact]
        public void ShouldReturnFalseForMultipleOperatorExpression()
        {
            string expression = "5**7";

            Assert.False(ExpressionValidator.IsValid(expression));
        }
    }
}
