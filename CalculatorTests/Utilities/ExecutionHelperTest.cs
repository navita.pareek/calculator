﻿using Xunit;
using Calculator;
namespace CalculatorTests
{
    public class ExecutionHelperTest
    {
        [Fact]
        public void ShouldReturnPriority3ForDivision()
        {
            Assert.Equal(3, ExecutionHelper.GetPriority('/'));
        }

        [Fact]
        public void ShouldReturnPriority2ForMultiplication()
        {
            Assert.Equal(2, ExecutionHelper.GetPriority('*'));
        }

        [Fact]
        public void ShouldReturnPriority1ForAddition()
        {
            Assert.Equal(1, ExecutionHelper.GetPriority('+'));
        }

        [Fact]
        public void ShouldReturnPriority0ForSubstraction()
        {
            Assert.Equal(0, ExecutionHelper.GetPriority('-'));
        }

        [Fact]
        public void ShouldReturnMinusOneForInvalidOperation()
        {
            Assert.Equal(-1, ExecutionHelper.GetPriority(']'));
        }
    }
}
