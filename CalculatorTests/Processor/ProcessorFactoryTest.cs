﻿using Xunit;
using Calculator;
namespace CalculatorTests
{
    public class ProcessorFactoryTest
    {
        [Fact]
        public void ShouldReturnTextFileProcessor()
        {
            var processor = ProcessorFactory.GetFactoryInstance().GetProcessorInstance(ProcessorType.TEXT_FILE);
            Assert.IsType<TextFileProcessor>(processor);
        }

        [Fact]
        public void ShouldReturnConsoleProcessor()
        {
            var processor = ProcessorFactory.GetFactoryInstance().GetProcessorInstance(ProcessorType.CONSOLE);
            Assert.IsType<ConsoleProcessor>(processor);
        }

        [Fact]
        public void ShouldReturnArgumentProcessor()
        {
            var processor = ProcessorFactory.GetFactoryInstance().GetProcessorInstance(ProcessorType.ARGUMENTS);
            Assert.IsType<ArgumentProcessor>(processor);
        }
    }
}
