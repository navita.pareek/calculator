﻿using Xunit;
using Calculator;
namespace CalculatorTests
{
    public class ExpressionExecutorTest
    {
        [Fact]
        public void ShouldReturnResultForValidExpression()
        {
            string expression = "7/5*2+8";

            Assert.Equal("10.8", (new ExpressionExecutor()).Execute(expression));
        }

        [Fact]
        public void ShouldReturnResultForNegativeExpressions()
        {
            string expression = "-2-2";
            var result = (new ExpressionExecutor()).Execute(expression);
            Assert.Equal("-4", (new ExpressionExecutor()).Execute(expression));
        }

        [Fact]
        public void ShouldReturnResultForPositiveStartingExpressions()
        {
            string expression = "+2+2";
            var result = (new ExpressionExecutor()).Execute(expression);
            Assert.Equal("4", (new ExpressionExecutor()).Execute(expression));
        }

        [Fact]
        public void ShouldReturnInputIfNoOperatorProvided()
        {
            string expression = "7.5";

            Assert.Equal("7.5", (new ExpressionExecutor()).Execute(expression));
        }

        [Fact]
        public void ShouldReturnErrorMessageForInvalidExpression()
        {
            string expression = "7/5*2+";

            Assert.Equal("Invalid Expression", (new ExpressionExecutor()).Execute(expression));
        }

        [Fact]
        public void ShouldReturnErrorMessageForEmptyExpression()
        {
            string expression = " ";

            Assert.Equal("Invalid Expression", (new ExpressionExecutor()).Execute(expression));
        }
    }
}
