﻿namespace Calculator
{
    using NLog;
    using System.IO;

    public class CalculatorManager
    {

        public void EvaluateExpressions(string[] args)
        {
            IProcessor processor;

            if (args == null || args.Length == 0)
            {
                processor = ProcessorFactory.GetFactoryInstance().GetProcessorInstance(ProcessorType.CONSOLE);
                processor.Execute();
            }
            for (int index = 0; index < args.Length; index++)
            {
                processor = getProcessor(args[index]);
                processor.Execute(args[index]);
            }
        }

        private IProcessor getProcessor(string arg)
        {
            if (File.Exists(arg) && arg.EndsWith(".txt"))
            {
                return ProcessorFactory.GetFactoryInstance().GetProcessorInstance(ProcessorType.TEXT_FILE);
            }
            else
            {
                return ProcessorFactory.GetFactoryInstance().GetProcessorInstance(ProcessorType.ARGUMENTS);
            }
        }
    }
}
