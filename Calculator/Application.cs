﻿using System;

namespace Calculator
{
    class Application
    {
        
        static void Main(string[] args)
        {
            Console.WriteLine(Resources.InfoMessageWelcome);
            var calculatorManager = new CalculatorManager();
            calculatorManager.EvaluateExpressions(args);
            Console.WriteLine(Resources.InfoMessageExit);
        }
    }
}
