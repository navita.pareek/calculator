﻿namespace Calculator
{
    using System;
    using System.IO;
    using System.Text;
    using NLog;

    public class TextFileProcessor : IProcessor
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public void Execute(string input)
        {
            string expressionInputFilename = input;
            if (!File.Exists(expressionInputFilename))
            {
               expressionInputFilename = Directory.GetCurrentDirectory() + Path.DirectorySeparatorChar + expressionInputFilename;

                if (!File.Exists(expressionInputFilename))
                {
                    logger.Error(Resources.ErrorMessageExpressionInputFileInvalid + expressionInputFilename);
                    Console.WriteLine(Resources.ErrorMessageExpressionInputFileInvalid + expressionInputFilename);
                    return;
                }
            }

            string result = getExpressionsResult(expressionInputFilename);
            writeResultToFile(result, expressionInputFilename);
        }

        private string getExpressionsResult(string expressionInputFilename)
        {
            Console.WriteLine(Resources.InfoMessageToReadTextFile + expressionInputFilename);
            StringBuilder stringBuilder = new StringBuilder();
            using (var streamReader = File.OpenText(expressionInputFilename))
            {
                string expression;
                var executionExecutor = new ExpressionExecutor();
                do
                {
                    expression = streamReader.ReadLine();
                    if (!string.IsNullOrEmpty(expression))
                    {
                        expression = expression.Replace(" ", string.Empty);
                        string result = executionExecutor.Execute(expression);
                        stringBuilder.Append(expression + " = " + result);
                        Console.WriteLine(expression + " = " + result);
                        stringBuilder.Append(Environment.NewLine);
                    }
                } while (!string.IsNullOrEmpty(expression));
            }

            return stringBuilder.ToString();
        }

        private void writeResultToFile(string results, string inputFilename)
        {
            string outputFilename = inputFilename.Replace(".txt", "_output.txt");
            Console.WriteLine(Resources.InfoMessageToWriteTextFile + outputFilename);

            IOutputWriter outputWriter = OutputWriterFactory.GetFactoryInstance().GetOutputWriterInstance(OutputType.TEXT_FILE);
            outputWriter.WriteOutput(results, outputFilename);
        }
    }
}
