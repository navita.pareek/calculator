﻿namespace Calculator
{
    using System;

    public class ConsoleProcessor : IProcessor
    {
        public void Execute(string input)
        {
            var executionExecutor = new ExpressionExecutor();
            Console.WriteLine(Resources.InfoMessageToStopCalculator);
            do
            {
                Console.Write("Provide Expression: ");
                input = Console.ReadLine();
                input = input.Replace(" ", string.Empty);
                if (!string.IsNullOrEmpty(input))
                {
                    string output = executionExecutor.Execute(input);
                    Console.WriteLine("Result = " + output);
                }
            } while (!string.IsNullOrWhiteSpace(input));
        }
    }
}
