﻿namespace Calculator
{
    using System;

    public class ArgumentProcessor : IProcessor
    {
        public void Execute(string input)
        {
            if (!string.IsNullOrEmpty(input))
            {
                var executionExecutor = new ExpressionExecutor();
                input = input.Replace(" ", string.Empty);
                string output = executionExecutor.Execute(input);
                Console.WriteLine(input + " = " + output);
            }
        }
    }
}
