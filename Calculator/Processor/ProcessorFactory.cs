﻿namespace Calculator
{
    using NLog;

    public class ProcessorFactory
    {
        private static ProcessorFactory _processorFactoryInstance;

        private IProcessor _processorInstance;

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        private ProcessorFactory()
        {
        }

        public static ProcessorFactory GetFactoryInstance()
        {
            if (_processorFactoryInstance == null)
                _processorFactoryInstance = new ProcessorFactory();

            return _processorFactoryInstance;
        }

        public IProcessor GetProcessorInstance(ProcessorType processorType)
        {
            if (processorType == ProcessorType.TEXT_FILE)
            {
                logger.Info(Resources.InfoMessageTextFileProcessorInitiated);
                _processorInstance = new TextFileProcessor();
            }
            else if(processorType == ProcessorType.ARGUMENTS)
            {
                logger.Info(Resources.InfoMessageArgumentProessorInitiated);
                _processorInstance = new ArgumentProcessor();
            }
            else
            {
                logger.Info(Resources.InfoMessageConsoleProcessorInitiated);
                _processorInstance = new ConsoleProcessor();
            }

            return _processorInstance;
        }
    }
}
