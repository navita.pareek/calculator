﻿namespace Calculator
{
    public interface IProcessor
    {
        void Execute(string input = null);
    }
}
