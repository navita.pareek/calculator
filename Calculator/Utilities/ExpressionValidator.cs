﻿using System;
namespace Calculator
{
    public static class ExpressionValidator
    {
        public static bool IsValid(string expression)
        {
            if (string.IsNullOrWhiteSpace(expression) || !isValidEnd(expression[expression.Length - 1]))
                return false;

            string num = string.Empty;
            for (int j = 0; j < expression.Length; j++)
            {
                // Read and evaluate the number, if present.
                num = string.Empty;
                while (j < expression.Length && ((expression[j] >= '0' && expression[j] <= '9') || expression[j] == '.'))
                {
                    num += expression[j];
                    j++;
                }

                if(!isDecimalNumber(num))
                {
                    return false;
                }

                // Read and evaluate the operator, if present.
                if (j < expression.Length)
                {
                    if (!isValidOperator(expression[j])
                        || (num == "" && !isValidInitialOperator(expression[j])))
                    {
                        return false;
                    }
                }
            }

            return !num.Equals(string.Empty);   // Expression should consists of at least 1 number.
        }

        private static bool isDecimalNumber(string num)
        {
            double result;
            return (num == "" || double.TryParse(num, out result));
        }

        private static bool isValidOperator(char op)
        {
            return (op == '/' || op == '*' || op == '+' || op == '-');
        }

        private static bool isValidInitialOperator(char op)
        {
            return (op == '+' || op == '-');    // Expression can start from '+' or '-'
        }

        private static bool isValidEnd(char last)
        {
            return (last >= '0' && last <= '9') || last == '.';
        }
    }
}
