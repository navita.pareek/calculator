﻿namespace Calculator
{
    public static class ExecutionHelper
    {
        public static int GetPriority(char op)
        {
            if (op == '/')
                return 3;

            if (op == '*')
                return 2;

            if (op == '+')
                return 1;

            if (op == '-')
                return 0;

            return -1;
        }

    }
}
