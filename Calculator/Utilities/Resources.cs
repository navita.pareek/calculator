﻿namespace Calculator
{
    public static class Resources
    {
        public const string InfoMessageWelcome = "Welcome to Calculator!!";
        public const string InfoMessageExit = "Calculator Stopped!!";
        public const string InfoMessageConsoleProcessorInitiated = "Console processor initiated";
        public const string InfoMessageArgumentProessorInitiated = "Argument processor initiated";
        public const string InfoMessageTextFileProcessorInitiated = "Text file processor initiated";
        public const string InfoMessageToReadTextFile = "Reading expressions from file: ";
        public const string InfoMessageToWriteTextFile = "Adding results to file: ";
        public const string InfoMessageToStopCalculator = "Empty expression will stop the calculator.";

        public const string ErrorMessageInvalidExpression = "Invalid Expression ";
        public const string ErrorMessageDivideByZeroException = "Invalid Expression as encountered divide by zero.";
        public const string ErrorMessageException = "Invalid Expression as encountered error: ";
        public const string ErrorMessageWriteOutputFailed = "Unable to write result to output file: ";
        public const string ErrorMessageExpressionInputFileInvalid = "Expressions input file not present: ";
    }
}
