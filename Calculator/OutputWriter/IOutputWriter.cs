﻿namespace Calculator
{
    public interface IOutputWriter
    {
        void WriteOutput(string outputToWrite, string filename = "output.txt");
    }
}
