﻿using System;

namespace Calculator
{
    using System.IO;
    using NLog;

    public class TextFileOutputWriter : IOutputWriter
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public void WriteOutput(string outputToWrite, string outputFilename)
        {
            try
            {
                File.WriteAllText(outputFilename, outputToWrite);
            }
            catch (Exception exception)
            {
                logger.Error(Resources.ErrorMessageWriteOutputFailed + outputFilename);
                Console.WriteLine(Resources.ErrorMessageWriteOutputFailed + outputFilename);
                logger.Error(exception);
            }
        }
    }
}
