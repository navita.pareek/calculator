﻿namespace Calculator
{
    public class OutputWriterFactory
    {
        private static OutputWriterFactory _outputWriterFactoryInstance;

        private IOutputWriter _outputWriterInstance;

        private OutputWriterFactory()
        {
        }

        public static OutputWriterFactory GetFactoryInstance()
        {
            if (_outputWriterFactoryInstance == null)
                _outputWriterFactoryInstance = new OutputWriterFactory();

            return _outputWriterFactoryInstance;
        }

        public IOutputWriter GetOutputWriterInstance(OutputType outputType)
        {
            if (outputType == OutputType.TEXT_FILE)
            {
                _outputWriterInstance = new TextFileOutputWriter();
            }

            return _outputWriterInstance;
        }
    }
}
