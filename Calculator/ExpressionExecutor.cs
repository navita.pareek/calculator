﻿namespace Calculator
{
    using System;
    using System.Collections.Generic;
    using NLog;

    public class ExpressionExecutor
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public string Execute(string expression)
        {
            if (!ExpressionValidator.IsValid(expression))
            {
                logger.Error(expression + " = " + Resources.ErrorMessageInvalidExpression);
                return Resources.ErrorMessageInvalidExpression;
            }

            Stack<double> numStack = new Stack<double>();
            Stack<char> opsStack = new Stack<char>();
            string num = string.Empty;

            for (int j = 0; j < expression.Length; j++)
            {
                // Read the number, if present.
                while (j < expression.Length && ((expression[j] >= '0' && expression[j] <= '9') || expression[j] == '.'))
                {
                    num += expression[j];
                    j++;
                }

                if (num != "")
                    numStack.Push(Double.Parse(num));

                // Always maintain stack with hightest priority operator at the top.
                // Perform top operation if read a lower priority operator or reached the end of the expression.
                while (opsStack.Count > 0
                    && (j == expression.Length
                        || (ExecutionHelper.GetPriority(expression[j]) < ExecutionHelper.GetPriority(opsStack.Peek()))))
                {
                    try
                    {
                        executeTopOperation(numStack, opsStack);
                    }
                    catch (DivideByZeroException)
                    {
                        logger.Error(expression + " = " + Resources.ErrorMessageDivideByZeroException);
                        return Resources.ErrorMessageDivideByZeroException;
                    }
                }

                if (j == expression.Length)
                    break;

                // Operator read is sign of the number
                if (numStack.Count == 0)
                    num = expression[j].ToString();
                else
                {
                    num = string.Empty;
                    opsStack.Push(expression[j]);
                }
            }

            var result = numStack.Pop().ToString();
            logger.Info(expression + " = " + result);
            return result;
        }

        void executeTopOperation(Stack<double> numStack, Stack<char> opsStack)
        {
            double num2 = numStack.Pop();
            double num1 = numStack.Pop();
            double result = evaluate(num1, opsStack.Pop(), num2);
            numStack.Push(result);
        }

        double evaluate(double num1, char op, double num2)
        {
            if (op == '/')
            {
                if (num2 != 0)
                    return num1 / num2;
                else
                    throw new DivideByZeroException();
            }

            if (op == '*')
                return num1 * num2;

            if (op == '+')
                return num1 + num2;

            if (op == '-')
                return num1 - num2;

            return -1;
        }
    }
}
