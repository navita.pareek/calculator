# Calculator

This is a simple calculator for evaluating arithmetic expressions. It can perform addition, substraction, multiplication and division. If an expression is provided with multiple operators then calculator follows BODMAS rule. Expressions can be provided either directly as arguments or through a file. If no argument is provided then calculator ask for expressions interactively.

## Tech

This app is created using C# on .net v5 platform. It consists of unit tests written using xTest. It uses NLog for logging events.

## Usage

Calculator expects arithmetic expressions as direct arguments, file or Concole.

### Expressions as Arguments

You can provide multiple argument directly as below. 

```
"2 + 3 * 7.5" "-5 * .2"
```

The result of these expressions is provided on console.

![Screenshot_2021-07-02_at_1.57.18_PM](/uploads/5b710831e48f4d9467481a96af3e68d4/Screenshot_2021-07-02_at_1.57.18_PM.png)

### Expressions in file

You can provide multiple argument by first adding these line seperated expressions to a text file and then passing the filename as an argument.

```
2 + 3 * 7.5 
-5 * .2
3.55 * 17.2 / 12
8 - 18 / 6
```

```
"SampleExpresssions.txt"
```
The result of these expressions is provided on console as well as added to a text file named "<inputExpressionsFilename>_output.txt" .

![Screenshot_2021-07-02_at_1.58.18_PM](/uploads/f380015adf845afca31b8e6cab3a4211/Screenshot_2021-07-02_at_1.58.18_PM.png)

You can also provide the combination of above two methods. Then arguments will be -

```
"2 + 3 * 7.5" "-5 * .2" "SampleExpresssions.txt"
```
![Screenshot_2021-07-02_at_1.58.58_PM](/uploads/1e7c7e46b52ded30d7177de1cd6cd254/Screenshot_2021-07-02_at_1.58.58_PM.png)

### Runtime Expressions

If no argument is provided to calculator then user can provide expressions runtime. Calculator keeps a loop to ask next expression until an empty expression is provided.

### Invalid Expressions

Caluculator doesnot process any input apart from valid decimal number, '+', '-', '*' and '/'. Any expressions consists of any other character is considered as invalid expression. Below are few examples of invalid expressions.

```
10 abc 20
abc + abc
2 ** 2
156 + 123.23.45
```

### Logging

With every execultion of Calculator, a log file is generated in Logs folder. Logfile is name is in format "CalculatorLog_<timestamp>.txt". This can be changed through NLog.config file.

![Screenshot_2021-07-02_at_2.12.00_PM](/uploads/0b4ef8716a0b43621ef5a76897689136/Screenshot_2021-07-02_at_2.12.00_PM.png)

